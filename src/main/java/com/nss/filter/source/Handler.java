////////////////////////////////////////////////////////////////////////////////
package com.nss.filter.source;
////////////////////////////////////////////////////////////////////////////////
import java.io.*;
import java.util.*;
import java.util.logging.*;
import static com.nss.filter.controller.FilterServlet.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
////////////////////////////////////////////////////////////////////////////////
public class Handler {
//------------------------------------------------------------------------------
    public String filterText() {
        if (getQuery().containsKey("q")) {
            final String SEARCH = (getQuery().get("q"))[0];
            if (!SEARCH.isEmpty()) {
                return limitTextLenght(searchWord(SEARCH));
            }
        }
        return limitTextLenght(readTextFromFile());
    }
//------------------------------------------------------------------------------
    private StringBuilder searchWord(final String SEARCH) {
        final StringBuilder SB = new StringBuilder();
        for (String str : parsingText(readTextFromFile().toString())) {
            if (str.contains(SEARCH)) {
                SB.append(limitWordLenght(str)).append(", ");
            }
        }
        return SB;
    }
//------------------------------------------------------------------------------ 
    private String limitWordLenght(String str) {
        if (getQuery().containsKey("length")) {
            final String LENGTH = getQuery().get("length")[0];
            if (!LENGTH.isEmpty()) {
                final int STRLENGTH = str.length();
                final int _LENGTH = Integer.parseInt(LENGTH);
                if (STRLENGTH <= _LENGTH) {
                    return str;
                } else {
                    return str.substring(0, _LENGTH);
                }
            }
        }
        return str;
    }
//------------------------------------------------------------------------------
    private String limitTextLenght(StringBuilder text) {
        if (getQuery().containsKey("limit")) {
            final String LIMIT = getQuery().get("limit")[0];
            if (!LIMIT.isEmpty()) {
                final int TEXTLENGTH = text.length();
                final int _LIMIT = Integer.parseInt(LIMIT);
                if (TEXTLENGTH > _LIMIT) {
                    return text.substring(0, _LIMIT);
                } else {
                    return text.toString();
                }
            }
        }
        return defaultLimitTextLenght(text);
    }
//------------------------------------------------------------------------------
    private String defaultLimitTextLenght(StringBuilder text) {
        final int TEXTLENGTH = text.length();
        if (TEXTLENGTH > 10000) {
            return text.substring(0, 10000);
        }
        return text.toString();
    }
//------------------------------------------------------------------------------
    private File getFile() {
        final StringBuilder SB = new StringBuilder();
        final Properties FILEPATH = new Properties();
        final InputStream IN = getClass().getClassLoader().getResourceAsStream(
                "filePath.properties");
        try {
            FILEPATH.load(IN);
        } catch (IOException e) {
            Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, e);
        }
        return new File((SB
                .append(FILEPATH.getProperty("file.prefix"))
                .append(getPathInfo().substring(1))
                .append(FILEPATH.getProperty("file.suffix")))
                .toString());
    }
//------------------------------------------------------------------------------
    public Map getFileMetaData() {
        final Map FILEMETADATA = new LinkedHashMap();

        final File FILE = getFile();
        final Path PATH = FILE.toPath();

        BasicFileAttributes attrs = null;
        try {
            attrs = Files.readAttributes(PATH, BasicFileAttributes.class);
        } catch (IOException ex) {
            Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        FILEMETADATA.put("fileName", FILE.getName());
        FILEMETADATA.put("fileSize", ((double) attrs.size()/1024 + " KB"));
        FILEMETADATA.put("fileCreationDate", attrs.creationTime());
        
        return FILEMETADATA;
    }
//------------------------------------------------------------------------------ 
    private StringBuilder readTextFromFile() {
        final StringBuilder SB = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(getFile()))) {
            String str = "";
            while ((str = br.readLine()) != null) {
                SB.append(str);
            }
        } catch (IOException ex) {
            Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return SB;
    }
//------------------------------------------------------------------------------
    private List<String> parsingText(String text) {
        final List<String> LISTWORDS = new ArrayList();
        StringTokenizer token = new StringTokenizer(text, ",. =[](){}!&?-|/'\"'");
        while (token.hasMoreTokens()) {
            LISTWORDS.add(token.nextToken());
        }
        return LISTWORDS;
    }
//------------------------------------------------------------------------------
}
////////////////////////////////////////////////////////////////////////////////