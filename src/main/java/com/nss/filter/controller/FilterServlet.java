////////////////////////////////////////////////////////////////////////////////
package com.nss.filter.controller;
////////////////////////////////////////////////////////////////////////////////
import com.nss.filter.source.Handler;
import java.io.IOException;
import java.util.*;
import java.util.logging.*;
import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.*;
import org.json.*;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
////////////////////////////////////////////////////////////////////////////////
@WebServlet(name = "QueryTextFile", urlPatterns = {"/*"})
public class FilterServlet extends HttpServlet {

    private static String PATHINFO = "";
    private static Map<String, String[]> QUERY = new LinkedHashMap();
//------------------------------------------------------------------------------    
    public static String getPathInfo() {
        return PATHINFO;
    }
//------------------------------------------------------------------------------    
    public static Map<String, String[]> getQuery() {
        return QUERY;
    }
//------------------------------------------------------------------------------
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");

        synchronized (this) {
            PATHINFO = request.getPathInfo();
            QUERY = request.getParameterMap();

            final Handler HAND = new Handler();
            final JSONObject JSON = new JSONObject();
            final JSONArray ARRAY = new JSONArray();
            
            try {
                JSON.put("RESPONSE", ARRAY.put(HAND.filterText()));
            } catch (JSONException ex) {
                Logger.getLogger(FilterServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (QUERY.containsKey("includeMetaData")) {
                String bool = QUERY.get("includeMetaData")[0];
                if (!bool.isEmpty()) {
                    if (Boolean.parseBoolean(bool)) {
                        try {
                            JSON.put("METADATA", HAND.getFileMetaData());
                        } catch (JSONException ex) {
                            Logger.getLogger(FilterServlet.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
            response.getWriter().println(JSON);
        }
    }
//------------------------------------------------------------------------------
}
////////////////////////////////////////////////////////////////////////////////